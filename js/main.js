var mask = {
    init: function () {
        var self = this;

        self._collectData();
        self._bindEvents();
    },

    _collectData: function () {
        var self = this;

        self._$maskedInputs = $('.js-mask');
    },

    _bindEvents: function () {
        var self = this;

        self._$maskedInputs.each(function () {
            var $input = $(this),
                mask = $input.data('mask');

            if (!mask)
                return;

            $input.mask(mask);
        });
    }
};

var welcomeForm = {
    _activeClass: 'b-active',
    _validClass: 'b-form__valid',
    _invalidClass: 'b-form__invalid',
    _errorElement: 'div',
    _successText: 'Спасибо!<br>В ближайшее время вы получите приглашение',
    _showSuccessNotifyTime: 4000,
    _showErrorNotifyTime: 2000,
    init: function () {
        var self = this;

        self._collectData();
        self._setValidate();
        self._resetForm();
    },

    _collectData: function () {
        var self = this;

        self._$form = $('#form-welcome');
        self._$notice = $('.js-notice');

        self._validator = null;
    },

    _setValidate: function () {
        var self = this;

        self._validator = self._$form.validate({
            focusInvalid: false,
            focusCleanup: true,
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            ignore: '',
            rules: {
                name: {
                    required: true
                },
                phone: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: 'Введите <b>имя</b>'
                },
                phone: {
                    required: 'Введите <b>телефон</b>'
                }
            },
            submitHandler: function () {
                var data = self._$form.serialize();

                self._sendRequest(data);
            },
            showErrors: function (errorMap) {
                var field,
                    errorsArr = [];

                for (field in errorMap) {
                    errorsArr.push(errorMap[field]);
                }

                if (errorsArr.length) {
                    self._showErrors(errorsArr.join('<br>'));

                }
            },
            errorElement: self._errorElement,
            validClass: self._validClass,
            errorClass: self._invalidClass
        });
    },

    _sendRequest: function (jsonData) {
        var self = this;

        self._showSuccessMessage();
        self._resetForm();

        //$.post(
        //    '/feedback/ajax/welcome',
        //    jsonData,
        //    function (data) {
        //        if (data == true) {
        //
        //        }
        //    }
        //);
    },

    _showSuccessMessage: function () {
        var self = this;

        helper.showNotice(self._successText, 'success');

        setTimeout(function () {
            helper.closeNotice();
        }, self._showSuccessNotifyTime);
    },

    _showErrors: function (errors) {
        var self = this;

        helper.showNotice(errors, 'error');

        setTimeout(function () {
            helper.closeNotice();
        }, self._showErrorNotifyTime);
    },

    _resetForm: function () {
        var self = this;

        self._$form.trigger('reset');
        self._validator.resetForm();
    }
};

var tabs = {
    _activeClass: 'b-active',
    init: function () {
        var self = this;

        self._collectData();
        self._bindEvents();
        self._initTab();
    },

    _collectData: function () {
        var self = this;

        self._$html = $('html, body');

        self._$tabs = $('.js-tabs');
        self._$tabsContent = $('.js-tabs-content');

        self._$switchers = self._$tabs.find('.js-tab');
        self._$contents = self._$tabsContent.find('.js-tab-content');

        self._tabsScrollTop = self._$tabs.offset().top - 100;
    },

    _bindEvents: function () {
        var self = this,
            $switcher,
            hash,
            $content;

        self._$switchers.each(function (index) {
            $switcher = $(this);
            hash = $switcher.data('hash');
            $content = self._$contents.eq(index);

            (function ($switcher, $content, hash) {
                $switcher.on('click', function () {
                    self._setHash(hash);
                    self._moveToServices();
                    self._setSwitcherActive($switcher);
                    self._showContent($content);
                });
            }($switcher, $content, hash));
        });
    },

    _setHash: function (hash) {
        var self = this;

        window.location.hash = hash;
    },

    _moveToServices: function () {
        var self = this;

        helper.moveTo(self._tabsScrollTop);
    },

    _setSwitcherActive: function ($switcher) {
        var self = this;

        self._$switchers.removeClass(self._activeClass);
        $switcher.addClass(self._activeClass);
    },

    _showContent: function ($content) {
        var self = this;

        self._$contents.removeClass(self._activeClass);
        $content.addClass(self._activeClass);
    },

    _initTab: function () {
        var self = this,
            hash = window.location.hash.substr(1),
            $switcher,
            $content;

        if (hash) {
            $switcher = self._$switchers.filter('[data-hash="' + hash + '"]');

            if ($switcher.length) {
                $content = self._$contents.eq($switcher.index());
                self._moveToServices();
            }
        }

        if (!$switcher || !$switcher.length) {
            $switcher = self._$switchers.eq(0);
            $content = self._$contents.eq(0);
        }

        self._setSwitcherActive($switcher);
        self._showContent($content);
    }
};

var accordion = {
    _activeClass: 'b-active',
    init: function () {
        var self = this;

        self._collectData();
        self._bindEvents();
        self._activateFirstItem();
    },

    _collectData: function () {
        var self = this;

        self._$accordion = $('.js-accordion');
        self._$items = self._$accordion.find('.js-item');

        self._$switchers = self._$items.find('.js-switcher');
    },

    _bindEvents: function () {
        var self = this,
            $switcher,
            $item;

        self._$switchers.each(function (index) {
            $switcher = $(this);
            $item = self._$items.eq(index);

            (function ($switcher, $item) {
                $switcher.on('click', function () {
                    self._setItemActive($item);
                });
            }($switcher, $item));
        });
    },

    _setItemActive: function ($item) {
        var self = this;

        self._$items.removeClass(self._activeClass);
        $item.addClass(self._activeClass);
    },

    _activateFirstItem: function () {
        var self = this,
            $item = self._$items.eq(0);

        self._setItemActive($item);
    }
};

var slider = {
    _inactiveClass: 'b-inactive',
    _visibleCount: 4,
    _speed: 300,
    init: function () {
        var self = this;

        self._collectData();
        self._bindEvents();
    },

    _collectData: function () {
        var self = this;

        self._$slider = $('.js-slider');
        self._$items = self._$slider.find('.js-item');
        self._itemsCount = self._$items.length;
        self._$prevBtn = self._$slider.siblings('.js-prev');
        self._$nextBtn = self._$slider.siblings('.js-next');
    },

    _bindEvents: function () {
        var self = this;

        if (self._itemsCount <= self._visibleCount) {
            self._$prevBtn.addClass(self._inactiveClass);
            self._$nextBtn.addClass(self._inactiveClass);
            return;
        }

        self._$slider.jCarouselLite({
            btnPrev: self._$prevBtn,
            btnNext: self._$nextBtn,
            visible: self._visibleCount,
            speed: self._speed
        });
    }
};

var menu = {
    init: function () {
        var self = this;

        self._collectData();
        self._bindEvents();
    },

    _collectData: function () {
        var self = this;

        self._$menu = $('.js-menu');
        self._$menuItems = self._$menu.find('.js-item');
        self._$sections = $('.js-section');
    },

    _bindEvents: function () {
        var self = this;

        self._$menuItems.on('click', function () {
            var section = $(this).data('section'),
                $section = self._$sections.filter('[data-section="' + section + '"]'),
                scrollTop;

            if ($section.length) {
                scrollTop = $section.offset().top;

                helper.moveTo(scrollTop);
            }
        });
    }
};

var helper = {
    _noticeClasses: {
        success: 'b-success',
        error: 'b-error'
    },
    _scrollDurationMs: 500,
    init: function () {
        var self = this;

        self._collectData();
        self._bindEvents();
    },

    _collectData: function () {
        var self = this;

        self._$html = $('html, body');
        self._$notice = $('.js-notice');
        self._$noticeMessage = $('.js-notice').find('.js-message');
    },

    _bindEvents: function () {
        var self = this;

        //self._$notice.modal({
        //    show: false,
        //    keyboard: false,
        //    backdrop: 'static'
        //});
    },

    _setNoticeType: function (type) {
        var self = this,
            noticeType;

        for (noticeType in self._noticeClasses) {
            if (type === noticeType) {
                self._$notice.addClass(self._noticeClasses[noticeType]);
            } else {
                self._$notice.removeClass(self._noticeClasses[noticeType]);
            }
        }
    },

    _setNoticeMessage: function (message) {
        var self = this;

        self._$noticeMessage.html(message);
    },

    moveTo: function (scrollTop) {
        var self = this;

        self._$html.animate({ scrollTop: scrollTop }, self._scrollDurationMs);
    },

    showNotice: function (message, type) {
        var self = this;

        type = type || 'success';
        self._setNoticeType(type);
        self._setNoticeMessage(message);

        self._$notice.modal('show');
    },

    closeNotice: function () {
        var self = this;

        self._$notice.modal('hide');
    }
};

$(function () {
    helper.init();
    menu.init();
    tabs.init();
    accordion.init();
    slider.init();
    mask.init();
    welcomeForm.init();
});